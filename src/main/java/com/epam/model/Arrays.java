package com.epam.model;

public class Arrays {

    private static final int SIZE = 10;
    private static final int DOUBLE_SIZE = SIZE*2;
    private static int[] array1 = new int[SIZE];
    private static int[] array2 = new int[SIZE];

    public Arrays() {
    }

    public static void fillArray() {
        for (int i = 0; i < SIZE; i++) {
            array1[i] = (int) (Math.random() * 10);
            array2[i] = (int) (Math.random() * 10);
        }
    }

    public static int summingArrays(int i) {
        int[] sum = new int[DOUBLE_SIZE];
        for (int j = 0; j < SIZE; j++) {
            sum[j] = array1[j];
        }
        for (int j = 0; j < SIZE; j++) {
            sum[j + SIZE] = array2[j];
        }
        return sum[i];
    }

    public static int deletingDuplicats(int k) {
        int[] sum = new int[DOUBLE_SIZE];
        int[] arrayWithoutDuplicates=new int[SIZE*2];
        for (int i = 0; i < DOUBLE_SIZE; i++){
            sum[i]=summingArrays(i);
        }
        for (int i=0;i<DOUBLE_SIZE;i++){
            for (int j=0;j<DOUBLE_SIZE;j++){
                if(sum[i]!=sum[j]){
                    if(j==DOUBLE_SIZE-1){
                        arrayWithoutDuplicates[i]=sum[i];
                    }else if (i==j){
                        arrayWithoutDuplicates[i]=sum[i];
                        break;
                    }
                }
            }
        }
        return arrayWithoutDuplicates[k];
    }


    public static void main(String[] args) {
        fillArray();
        for (int i = 0; i < SIZE; i++)
            System.out.println(array1[i]);
        System.out.println("-------------------");
        for (int i = 0; i < SIZE; i++)
            System.out.println(array2[i]);
        System.out.println("-------------------");
        for (int i = 0; i < DOUBLE_SIZE; i++)
            System.out.println(summingArrays(i));
        System.out.println("-------------------");
        for (int i = 0; i < DOUBLE_SIZE; i++)
            System.out.println(deletingDuplicats(i));
    }

}